# Flowgame

## Auteurs
Franklin LALERE et Xie JINGYI

## Résumé du projet

L'objectif de ce projet ludique est de recréer un flowgame en Java. Les fonctionnalités réalisées sont
les suivantes : 
* Sur une grille 6x6, le placement des cases est aléatoire (cycle eulérien)
* détection d'une liaison entre 2 cases de même symbole
* vider tout les chemins existants avec l'item "reset links"
* changer la taille de la grille actuelle (3x3 ou 6x6)
* une seule grille de taille 3x3 existe, chargée par la lecture d'un fichier texte
* détection d'une grille complétée et en informe le joueur
* L'application comporte une barre de menu avec 3 item : 2 item (reset links + new position) + 1 sous-menu de 2 item (changement de taille)
* Le chemin actuelle est rempli en maintenant le clic gauche sur les cases et détruit lors du relâchement du clic

# Compilation, Nettoyage et Exécution

## Dépendance(s)
java 17 (openjdk-17)

En premier lieu, allez à la racine du projet via la commande suivante:
```bash
git clone https://forge.univ-lyon1.fr/p1802899/flowgame.git
cd flowgame
```
## Compilation
Pour compiler les sources du projet:
```bash
make
```

## Nettoyage
Pour supprimer les fichier .class générés:
```bash
make clean
```

## Exécution
Pour exécuter le projet:
```bash
make exec
```



