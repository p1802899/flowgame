import java.util.Random;

public class CaseM {
    private CaseType type;
    private static Random rnd = new Random();
    private boolean linked;

    public CaseM() {
        type = CaseType.empty;
        linked = false;
    }

    public CaseM(CaseType t) {
        type = t;
        linked = false;
    }

    public void setLinked(boolean b) {
        linked = b;
    }

    public void setType(CaseType t) {
        type = t;
    }

    public void rnd() {
        if (type != CaseType.S1 && type != CaseType.S2) {
            //genere un type de fleche (dans type entre 6 et 10)
            int index = rnd.nextInt(4) + 6;
            type = CaseType.values()[index];
        }
    }

    public boolean isLinked() {
        return linked;
    }

    CaseType getType() { return type; }

}
