import java.awt.*;
import java.util.ArrayList;
import java.util.List;


public class Chemin {
    //liste des cases parcourus tant que l'utilisateur maintient le clic gauche enfoncé
    //lorsq'il relache la souris, la liste est alors vidée
    private List<Pair<CaseM, Point>> tabCaseChemin;

    //quand le chemin est lié,
    private boolean lie;

    public Chemin() {
        tabCaseChemin = new ArrayList<Pair<CaseM, Point>>();
        lie = false;
    }

    public boolean estSurPoint(Point p) {
        for (int i = 0; i < tabCaseChemin.size(); i++) {
            if (tabCaseChemin.get(i).getR().equals(p)) {
                return true;
            }
        }
        return false;
    }

    //renvoi vrai sur le point p fait partie d'un chemin lié
    public boolean estLieViaCePoint(Point p) {

        return (lie && estSurPoint(p));

    }

    public void clearGrille(CaseM [][]tabCM) {
        //supprime les liens du chemin dans la grille de notre modèle
        for (int i = 0; i < tabCaseChemin.size(); i++) {
            Point p = tabCaseChemin.get(i).getR();
            //si cette case possède un lien
            if (tabCM[p.x][p.y].getType() == CaseType.h0v1 || tabCM[p.x][p.y].getType() == CaseType.h1v1 ||
                    tabCM[p.x][p.y].getType() == CaseType.h1v0 || tabCM[p.x][p.y].getType() == CaseType.h0v0 ||
                    tabCM[p.x][p.y].getType() == CaseType.h0h1 || tabCM[p.x][p.y].getType() == CaseType.v0v1) {
                tabCM[p.x][p.y].setType(CaseType.empty);
            }
            tabCM[p.x][p.y].setLinked(false);
        }

        lie = false;
    }

    public void addCaseM(Point p, CaseM m, CaseM[][] tab) {
        //System.out.println("ajout de la case " + p.x + ", " + p.y + " au chemin");

        //ajout de la case au chemin si la première case du chemin est bien un symbole
        //tabCaseChemin.add(new Pair<CaseM, Point>(m, p));
        //il faut que la taille du chemin soit supérieur ou égale à 1 pour ensuite acceder au premier element de tabCaseChemin
        if (tabCaseChemin.size() > 0) {
            //si le type de la case à ajouté est le même que le type de la premiere case du chemin, alors le lien est validé,
            // on verifi que la position des 2 cases sont différentes et
            if ((m.getType() == tabCaseChemin.get(0).getL().getType()) && (p != tabCaseChemin.get(0).getR()))
            {
                //System.out.println("Le chemin est lié");
                m.setLinked(true);

                //tout les cases du chemin sont marqués lien, ce qui sert à l'affichage
                for (int i = 0; i < tabCaseChemin.size(); i++) {
                    Point pointLie = tabCaseChemin.get(i).getR();
                    //System.out.println("le point " + pointLie.x + ", " + pointLie.y + " du chemin est marqué comme lié");
                    tab[pointLie.x][pointLie.y].setLinked(true);
                }
                tab[p.x][p.y].setLinked(true);

                lie = true;
                return;
            }
        }

        //ajout de la case au chemin si la première case du chemin est bien un symbole
        tabCaseChemin.add(new Pair<CaseM, Point>(m, p));
    }

    //retourne le point de liaison entre 2 cases et le type de liaison
    public Pair<Point, CaseType> addLinks(Point p1, CaseM m) {
        Point p2, p3;
        Pair<Point, CaseType> link = new Pair<>(new Point(0, 0), CaseType.empty);
        Point translation1 = new Point(0, 0);
        Point translation2 = new Point(0, 0);
        //il faut qu'il y est au moins 2 point
        if (tabCaseChemin.size() > 1) {
            p2 = tabCaseChemin.get(tabCaseChemin.size() - 1).getR();
            p3 = tabCaseChemin.get(tabCaseChemin.size() - 2).getR();

            //coordonnées du vecteur qui va de p1 à p2
            translation1.x = p2.x - p1.x;
            translation1.y = p2.y - p1.y;

            //System.out.println("translation de p1 à p2 : " + translation1.x + ", " + translation1.y);

            //coordonnées du vecteur qui va de p2 à p3
            translation2.x = p3.x - p2.x;
            translation2.y = p3.y - p2.y;

            //System.out.println("translation de p2 à p3 : " + translation2.x + ", " + translation2.y);

            //on on set le point de liaison
            link.setL(p2);

            //les différents cas de rotation
            //de la gauche vers le bas et vice versa
            if ((translation1.x == -1 && translation1.y == 0 && translation2.x == 0 && translation2.y == -1) ||
                    (translation1.x == 0 && translation1.y == 1 && translation2.x == 1 && translation2.y == 0)) {
                //System.out.println("");
                link.setR(CaseType.h0v1);

            }
            //de la gauche vers le haut et vice versa
            else if ((translation1.x == 1 && translation1.y == 0 && translation2.x == 0 && translation2.y == -1) ||
                    (translation1.x == 0 && translation1.y == 1 && translation2.x == -1 && translation2.y == 0)){
                link.setR(CaseType.h0v0);
            }
            //de la droite vers le bas et vice versa
            else if ((translation1.x == -1 && translation1.y == 0 && translation2.x == 0 && translation2.y == 1) ||
                    (translation1.x == 0 && translation1.y == -1 && translation2.x == 1 && translation2.y == 0)) {

                link.setR(CaseType.h1v1);
            }
            //de la droite vers le haut et vice versa
            else if ((translation1.x == 1 && translation1.y == 0 && translation2.x == 0 && translation2.y == 1) ||
                    (translation1.x == 0 && translation1.y == -1 && translation2.x == -1 && translation2.y == 0)) {

                link.setR(CaseType.h1v0);
            }
            //de la gauche vers la droite et vise versa
            else if ((translation1.x == 0 && translation1.y == -1 && translation2.x == 0 && translation2.y == -1) ||
                    (translation1.x == 0 && translation1.y == 1 && translation2.x == 0 && translation2.y == 1)) {
                link.setR(CaseType.h0h1);
            }
            //du bas vers le haut et vise versa
            else if ((translation1.x == -1 && translation1.y == 0 && translation2.x == -1 && translation2.y == 0) ||
                    (translation1.x == 1 && translation1.y == 0 && translation2.x == 1 && translation2.y == 0)){
                link.setR(CaseType.v0v1);
            }
            else {
                link.setR(CaseType.empty);
            }

        }
        return link;
    }

    public void clear() {
        //System.out.println("effacement de la liste et du lien s'il existe");
        //tabCaseChemin.clear();
        lie = false;
    }

    public void display() {
        System.out.println("chemin :");
        Point p;
        CaseType t;
        for (int i = 0; i < tabCaseChemin.size(); i++) {
            p = tabCaseChemin.get(i).getR();
            t = tabCaseChemin.get(i).getL().getType();
            System.out.println("indice " + i + "(" + p.x + ", " + p.y + ", " + t + ")");

        }
    }

    public boolean estLie() {
        return lie;
    }

}
