import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observable;
import java.util.Observer;

public class Panel extends JPanel implements Observer {
    private JButton boutonSuivant;
    private Jeu game;
    private VueControleurGrille vue_controleur;

    public Panel(Jeu g, VueControleurGrille vc) {
        //super();
        game = g;
        vue_controleur = vc;

        game.addObserver(this);


        setSize(game.getTailleGrille() * vue_controleur.PIXEL_PER_SQUARE, game.getTailleGrille() * vue_controleur.PIXEL_PER_SQUARE);


        boutonSuivant = new JButton("You win !!!");
        boutonSuivant.setEnabled(true);

        boutonSuivant.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            }
        });

        add(boutonSuivant);
        setLayout(new BorderLayout());

    }

    public void load() {
        //new BorderLayout()
        int boutonLargeur = 80;
        int boutonLongueur = 150;

        //System.out.println("taille de la grille : " + game.getTailleGrille());
        int x = (game.getTailleGrille() * vue_controleur.PIXEL_PER_SQUARE - boutonLongueur)/2;
        int y = (game.getTailleGrille() * vue_controleur.PIXEL_PER_SQUARE - boutonLargeur)/2 - 5;

        //System.out.println("Position du bouton : " + x + ", " + y);

        boutonSuivant.setBounds(x, y, boutonLongueur, boutonLargeur);
    }


    @Override
    public void paintComponents(Graphics g) {
        super.paintComponents(g);

    }

    @Override
    public void update(Observable o, Object arg)
    {
        //System.out.println("je passe dans ce bout de code");
        boutonSuivant.repaint();
    }

}
