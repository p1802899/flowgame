import javax.swing.*;
import java.awt.*;
import java.awt.geom.Rectangle2D;
import java.util.Random;

// TODO : redéfinir la fonction hashValue() et equals(Object) si vous souhaitez utiliser la hashmap de VueControleurGrille avec VueCase en clef

public class VueCase extends JPanel {
    private int x, y;
    private CaseM model;

    public VueCase(int _x, int _y, CaseM m) {
        x = _x;
        y = _y;

        model = m;

    }

    private void checkCaseLinked(Graphics g) {
        Color cLinked = new Color(0, 150, 0);
        Color cNotLinked = new Color(200, 0, 0);
        if (model.isLinked()) {
            g.setColor(cLinked);
        }
        else {
            g.setColor(cNotLinked);
        }
    }

    private void drawNoon(Graphics g) {

        //g.drawLine(getWidth() / 2, getHeight() / 2, getWidth() / 2, 0);
        g.fillRoundRect(27, -3, 5, 30, 2, 2);
    }

    private void drawNine(Graphics g) {

        //g.drawLine(0, getHeight() / 2, getWidth() / 2, getHeight() / 2);
        g.fillRoundRect(-3, 22, 35, 5, 2, 2);
    }

    private void drawSix(Graphics g) {

        //g.drawLine(getWidth() / 2, getHeight() / 2, getWidth() / 2, getHeight());
        //g.drawLine


        g.fillRoundRect(27, 25, 5, 35, 2, 2);
    }

    private void drawThree(Graphics g) {

        //g.drawLine(getWidth() / 2, getHeight() / 2, getWidth(), getHeight() / 2);
        //g.drawRoundRect(27, 30, 30, 5, 2, 2);
        g.fillRoundRect(27, 22, 35, 5, 2, 2);
    }


    @Override
    public void paintComponent(Graphics g) {

        g.clearRect(0, 0, getWidth(), getHeight());

        g.drawRoundRect(getWidth() / 4, getHeight() / 4, getWidth() / 2, getHeight() / 2, 5, 5);
        //g.fillOval(getWidth() / 4, getHeight() / 4, getWidth() / 2, getHeight() / 2);


        Rectangle2D deltaText = g.getFont().getStringBounds("0", g.getFontMetrics().getFontRenderContext()); // "0" utilisé pour gabarit



        switch (model.getType()) {
            //Les cases entre 1 et 5
            case S1:
                checkCaseLinked(g);
                g.drawString("1", getWidth() / 2 - (int) deltaText.getCenterX(), getHeight() / 2 - (int) deltaText.getCenterY());
                break;
            case S2:
                checkCaseLinked(g);
                g.drawString("2", getWidth() / 2 - (int) deltaText.getCenterX(), getHeight() / 2 - (int) deltaText.getCenterY());
                break;
            case S3:
                checkCaseLinked(g);
                g.drawString("3", getWidth() / 2 - (int) deltaText.getCenterX(), getHeight() / 2 - (int) deltaText.getCenterY());
                break;
            case S4:
                checkCaseLinked(g);
                g.drawString("4", getWidth() / 2 - (int) deltaText.getCenterX(), getHeight() / 2 - (int) deltaText.getCenterY());
                break;
            case S5:
                checkCaseLinked(g);
                g.drawString("5", getWidth() / 2 - (int) deltaText.getCenterX(), getHeight() / 2 - (int) deltaText.getCenterY());
                break;

            case h0v0:
                checkCaseLinked(g);
                drawNine(g);
                drawNoon(g);
                break;

            case h0v1:
                checkCaseLinked(g);
                drawNine(g);
                drawSix(g);
                break;

            case h1v0:
                checkCaseLinked(g);
                drawNoon(g);
                drawThree(g);
                break;

            case h1v1:
                checkCaseLinked(g);
                drawThree(g);
                drawSix(g);
                break;

            case h0h1:
                checkCaseLinked(g);
                drawThree(g);
                drawNine(g);
                break;

            case v0v1:
                checkCaseLinked(g);
                drawNoon(g);
                drawSix(g);
                break;

            case cross:
                checkCaseLinked(g);
                drawNoon(g);
                drawSix(g);
                drawThree(g);
                drawNine(g);
                break;

        }
    }

    public String toString() {
        return x + ", " + y;

    }

}
