import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;
import java.awt.*;


public class BareDeMenu extends JMenuBar implements ActionListener {
    private JMenu menu, submenu;
    private final JMenuItem petiteGrilleItem, grandeGrilleItem, resetGrilleItem, newPositionsItem;
    private Jeu game;
    private VueControleurGrille vue_controleur;

    public BareDeMenu(Jeu g, VueControleurGrille vc) {
        super();

        game = g;
        vue_controleur = vc;

        menu = new JMenu("Préférences");
        submenu = new JMenu("Switch size");

        petiteGrilleItem = new JMenuItem("3x3");
        grandeGrilleItem = new JMenuItem("6x6");
        resetGrilleItem = new JMenuItem("reset links");
        newPositionsItem = new JMenuItem("new positions");

        //association des JMenuItem au listener
        petiteGrilleItem.addActionListener(this);
        grandeGrilleItem.addActionListener(this);
        resetGrilleItem.addActionListener(this);
        newPositionsItem.addActionListener(this);

        submenu.add(petiteGrilleItem);
        submenu.add(grandeGrilleItem);

        menu.add(resetGrilleItem);
        menu.add(newPositionsItem);

        menu.add(submenu);

        add(menu);
    }

    //clic sur les items de la barre de menu
    @Override
    public void actionPerformed(ActionEvent e) {

        //changement de taille de la grille en 3x3 ou 6x6
        if ((e.getSource() == petiteGrilleItem) && (game.getCurentGrilleIndex() != 0)) {
            //System.out.println("la taille de la grille passe en 3x3");
            vue_controleur.switchGrille(0);
        }

        if ((e.getSource() == grandeGrilleItem) && (game.getCurentGrilleIndex() != 1)) {
            //System.out.println("la taille de la grille passe en 6x6");
            vue_controleur.switchGrille(1);
        }

        //
        if (e.getSource() == resetGrilleItem) {
            //System.out.println("On reset la grille");
            //on supprime tout les chemins relié
            game.videToutLesChemins();
        }

        if (e.getSource() == newPositionsItem) {
            if (game.getCurentGrilleIndex() == 1) {
                //System.out.println("nouvelle position des cases pour la grille 6x6");
                game.videToutLesChemins();
                //game.loadGrille();
                game.effaceGrille();
                game.genereCaseAlea();
                //vue_controleur.initGrille();
            }
        }

    }
}
