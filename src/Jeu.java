import java.awt.*;
import java.io.BufferedReader;
import java.net.Inet4Address;
import java.nio.Buffer;
import java.sql.Array;
import java.util.*;
import java.util.List;
import java.nio.file.Files;
import java.nio.file.Path;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.*;

//Model
public class Jeu extends Observable {
    private static Random rnd = new Random();
    private CaseM[][] tabCM;
    private int tailleGrille;

    private final Point[] cycleEulerien; //tableau constant de 36 points représentant le cycle eulérien
    private int nbCouples; //nombre de couples de chiffres possibles à modifier avec CaseType.java
    //liste dynamique de chemins
    private List<Chemin> chemins;
    Chemin cheminActuel;
    private int cheminActuelId; //vaut -1
    private boolean boutonEstEnfonce;
    private Point casePositionPressed;
    private CaseM caseTypePressed;
    private int currentGrilleId;



    public Jeu(int size) {
        tailleGrille = size;

        //en fonction Type de la grille on lui associe un id (on peut faire un enum)
        if (tailleGrille == 3)
            currentGrilleId = 0;
        else if (tailleGrille == 6)
            currentGrilleId = 1;

        chemins = new ArrayList<>();
        //on cré un chemin vide existant de base
        chemins.add(new Chemin());
        cheminActuelId = 0; //indice du chemin actuel (dans la liste chemins)qui est tracé dans

        boutonEstEnfonce = false;
        //le cycle eulérien formé par un parcours de 36 points

        cycleEulerien = new Point[36];

        //points du cycle eulérien
        cycleEulerien[0] = new Point(0, 0); cycleEulerien[1] = new Point(1, 0); cycleEulerien[2] = new Point(2, 0);
        cycleEulerien[3] = new Point(2, 1); cycleEulerien[4] = new Point(2, 2); cycleEulerien[5] = new Point(3, 2);
        cycleEulerien[6] = new Point(3, 1); cycleEulerien[7] = new Point(3, 0); cycleEulerien[8] = new Point(4, 0);
        cycleEulerien[9] = new Point(5, 0); cycleEulerien[10] = new Point(5, 1); cycleEulerien[11] = new Point(4, 1);
        cycleEulerien[12] = new Point(4, 2); cycleEulerien[13] = new Point(5, 2); cycleEulerien[14] = new Point(5, 3);
        cycleEulerien[15] = new Point(4, 3); cycleEulerien[16] = new Point(3, 3); cycleEulerien[17] = new Point(3, 4);
        cycleEulerien[18] = new Point(4, 4); cycleEulerien[19] = new Point(5, 4); cycleEulerien[20] = new Point(5, 5);
        cycleEulerien[21] = new Point(4, 5); cycleEulerien[22] = new Point(3, 5); cycleEulerien[23] = new Point(2, 5);
        cycleEulerien[24] = new Point(2, 4); cycleEulerien[25] = new Point(2, 3); cycleEulerien[26] = new Point(1, 3);
        cycleEulerien[27] = new Point(1, 4); cycleEulerien[28] = new Point(1, 5); cycleEulerien[29] = new Point(0, 5);
        cycleEulerien[30] = new Point(0, 4); cycleEulerien[31] = new Point(0, 3); cycleEulerien[32] = new Point(0, 2);
        cycleEulerien[33] = new Point(1, 2); cycleEulerien[34] = new Point(1, 1); cycleEulerien[35] = new Point(0, 1);
    }

    public void setTypeGrille(int indice) {
        currentGrilleId = indice;

        videToutLesChemins();

        loadGrille();

        setChanged();
        notifyObservers();

    }

    public void loadGrille() {

        //chargement d'une grille 3x3 à partir d'un fichier
        if (currentGrilleId == 0) {
            tabCM = new CaseM[3][3];
            tailleGrille = 3;
            nbCouples = 2;

            try {
                //fichier d'entrée
                File f = new File("./data/grid0.txt");
                FileReader fr = new FileReader(f);

                BufferedReader br = new BufferedReader(fr);
                StringBuffer sb = new StringBuffer();

                String line;
                String[] casesLine;

                CaseType t;
                int lineIndex = 0;
                while ((line = br.readLine()) != null) {
                    sb.append(line);
                    casesLine = line.split(" ");
                    //System.out.println("taille de la ligne " + lineIndex + " : " + casesLine.length);

                    for (int i = 0; i < casesLine.length; i++) {
                        t = CaseType.valueOf(casesLine[i]);

                        //casesLine = CaseType.val
                        tabCM[lineIndex][i] = new CaseM(t);
                    }
                    lineIndex++;

                    sb.append("\n");
                }

                fr.close();
                //System.out.println("Contenu du fichier:");
                //System.out.println(sb.toString());
            }
            catch (IOException e) {
                System.err.println("Erreur lors de la lecture du fichier grid.txt");

                //si aucun fichier n'est disponible alors
                tabCM[0][0] = new CaseM(CaseType.S1);
                tabCM[0][1] = new CaseM(CaseType.empty);
                tabCM[0][2] = new CaseM(CaseType.S2);
                tabCM[1][0] = new CaseM(CaseType.empty);
                tabCM[1][1] = new CaseM(CaseType.empty);
                tabCM[1][2] = new CaseM(CaseType.empty);
                tabCM[2][0] = new CaseM(CaseType.empty);
                tabCM[2][1] = new CaseM(CaseType.S1);
                tabCM[2][2] = new CaseM(CaseType.S2);
            }

        } else if (currentGrilleId == 1) {
            tailleGrille = 6;
            nbCouples = 6;
            tabCM = new CaseM[6][6];

            for (int x = 0; x < tailleGrille; x++) {
                for (int y = 0; y < tailleGrille; y++) {
                    tabCM[x][y] = new CaseM(CaseType.empty);
                }
            }

            genereCaseAlea();
        }
    }

    // algorythme de placement des cases numéroté utilisant le cycle eulérien

    private boolean estMauvaisCouple(List<Point> l, Point p1, Point p2) {
        for (int i = 0; i < l.size(); i++) {
            Point p = l.get(i);
            //si au moins un des 2 points est à adjaçant à p
            if ((Math.abs(p1.x - p.x) == 1 && Math.abs(p1.y - p.y) == 0) || (Math.abs(p1.x - p.x) == 0 && Math.abs(p1.y - p.y) == 1) ||
            (Math.abs(p2.x - p.x) == 1 && Math.abs(p2.y - p.y) == 0) || (Math.abs(p2.x - p.x) == 0 && Math.abs(p2.y - p.y) == 1)) {
                return true;
            }

        }
        return false;
    }

    public void genereCaseAlea() {
        if (currentGrilleId == 1) {
            System.out.println("Génération des cases numéroté de 1 à 5 sur le terrain en cours...");
            int r;

            //génération aléatoire de nbCouples points sur le chemin
            Point p1, p2;

            //on regroupe dans une liste, "nbCouples" de points adjacents sur le cycle eulérien
            List<Point> pointsSurChemins = new ArrayList<>();
            for (int i = 0; i < nbCouples; i++) {

                //on génère 2 points adjacents
                do {
                    r = rnd.nextInt(35);

                    p1 = cycleEulerien[r];
                    p2 = cycleEulerien[r + 1];


                    //on vérifi qu'il n'y a pas de points adjacent au couple
                } while (pointsSurChemins.contains(p1) || pointsSurChemins.contains(p2) || estMauvaisCouple(pointsSurChemins, p1, p2));

                //on ajoute ce couple à la liste
                pointsSurChemins.add(p1);
                pointsSurChemins.add(p2);


            }

            //parcours des points
            int typeId = 1;
            for (int i = 0; i < pointsSurChemins.size(); i++) {
                int x = pointsSurChemins.get(i).x;
                int y = pointsSurChemins.get(i).y;

                //si c'est impair
                if (i % 2 != 0) {
                    typeId++;
                }

                if (typeId > nbCouples) {
                    typeId = 1;
                }

                CaseType t = CaseType.values()[typeId];

                //System.out.println("position du point " + i + " : " + x + ", " + y + " type : " + t);

                tabCM[x][y].setType(t);

                setChanged();
                notifyObservers();

            }

            System.out.println("Génération des cases numéroté réalisé");
        }
    }


    public void rnd(int i, int j) {
        tabCM[i][j].rnd();
        setChanged();
        notifyObservers();
    }

    //vérifi sur le chemin actuel est lié
    public boolean cheminValide() {
        return cheminActuel.estLie();
    }



    //vide le chemin actuel
    public void videChemin() {

        //on vide les cases avec un symbole de liason si le joueur relache son clic

        cheminActuel.clearGrille(tabCM);
        setChanged();
        notifyObservers();

        cheminActuel.clear();

        //cheminActuelId = 0;

    }

    public void videToutLesChemins() {
        //videChemin();

        for (int i = 0; i < chemins.size(); i++) {
            chemins.get(i).clearGrille(tabCM);
        }

        setChanged();
        notifyObservers();

        //System.out.println("Nombre de chemins avant la réinitialisation : " + chemins.size());

        //suppression de tout les chemins existants
        chemins.clear();

        cheminActuelId = 0;
        chemins.add(new Chemin());
        cheminActuel = chemins.get(0);

    }

    public void setIsPressed(boolean b) {
        boutonEstEnfonce = b;
    }

    public int chercheUnCheminLibre(Point p) {
        //on ne peut pas posé commencé un chemin par une case déjà occupée

        //System.out.println("Nombre de chemins existants : " + chemins.size());
        for (int indice = 0; indice < chemins.size(); indice++) {
            Chemin c = chemins.get(indice);
            //si le point p est sur ce chemin et que ce chemin est lié, alors le chemin n'est plus considéré comme lié
            if (c.estSurPoint(p) && c.estLie()) {
                return indice;
            }
        }
        return -1;
    }

    int getSearchCheminIdFromPoint(Point p) {
        for (int cheminId = 0; cheminId < chemins.size(); cheminId++) {
            if (chemins.get(cheminId).estLieViaCePoint(p)) {
                return cheminId;
            }
        }
        return -1;
    }

    public void setCasePressed(Point p, CaseM m) {
        casePositionPressed = p;
        caseTypePressed = m;
        cheminActuel = chemins.get(cheminActuelId);

        //on ajoute la case au chemin si le bouton est enfoncé et que la case est libre
        if (boutonEstEnfonce) {

            int cheminLibreId = chercheUnCheminLibre(p);


            //s'il existe un chemin existant pour ce point, alors on réutilise ce chemin en l'effaçant

            if (cheminLibreId != -1) {
                //System.out.println(""cheminLibreId);
                cheminActuelId = cheminLibreId;
                //System.out.println("Effacement du chemin lié d'indice : " + cheminLibreId);
                //cheminActuel = chemins.get(cheminActuelId);
                //cheminActuel = chemins.get(cheminActuelId);
                cheminActuel.clear();
                cheminActuel.clearGrille(tabCM);

                setChanged();
                notifyObservers();
            }

            //int c = getSearchCheminIdFromPoint(p);

            //lorsqu'un chemin est valide, alors on le stocke dans la liste et on en cré un nouveau en changeant de chemin actuel
            if (cheminActuel.estLie() && !cheminActuel.estSurPoint(p)) {
                //System.out.println("Création d'un nouceau chemin");
                chemins.add(new Chemin());
                cheminActuelId++;
                cheminActuel = chemins.get(cheminActuelId);

            }

            //récupère le type de lien
            Pair<Point, CaseType> lien = cheminActuel.addLinks(p, m);
            Point pointDirection = lien.getL();

            //on modifi le point de liaison sur la grille
            if (lien.getR() != CaseType.empty) {
                //System.out.println("le lien d'entre deux est un " + lien.getR());

                //on modifi la case dans le model puis on informe l'observeur
                tabCM[pointDirection.x][pointDirection.y].setType(lien.getR());
                setChanged();
                notifyObservers();
            }

            //afficheGrille();

            cheminActuel.addCaseM(p, m, tabCM);
            //cheminActuel.display();
        }
    }

    public void afficheGrille() {

        System.out.println("Contenu de la grille :");
        for (int x = 0; x < tailleGrille; x++) {
            for (int y = 0; y < tailleGrille; y++) {
                System.out.print(tabCM[x][y].getType().toString() + " ");
            }
            System.out.println("");
        }
    }

    void effaceGrille() {
        for (int x = 0; x < tailleGrille; x++) {
            for (int y = 0; y < tailleGrille; y++) {
                tabCM[x][y].setType(CaseType.empty);
                tabCM[x][y].setLinked(false);
            }
        }

    }

    public boolean isPressed() {
        return boutonEstEnfonce;
    }

    public int getCheminActuelId() {
        return cheminActuelId;
    }

    public int getNbCasesUtilises() {
        int nbcaseUtilise = 0;

        for (int x = 0; x < tailleGrille; x++) {
            for (int y = 0; y < tailleGrille; y++) {
                if (tabCM[x][y].getType() != CaseType.empty)
                    nbcaseUtilise++;
            }
        }
        return nbcaseUtilise;
    }


    public boolean partieGagne() {
        int nbCaseTotal = tailleGrille * tailleGrille;
        int nbCasesUtilise = getNbCasesUtilises();

        //vérifier que le nombre de couples est égale au nombre de chemins
        if ((chemins.size() != nbCouples) || (nbCasesUtilise != nbCaseTotal)) {
            return false;
        }
        for (int i = 0; i < chemins.size(); i++) {
            if (!chemins.get(i).estLie()) {
                return false;
            }
        }

        return true;
    }


    public CaseM getCase(int i, int j) {
        return tabCM[i][j];
    }

    public int getTailleGrille() {
        return tailleGrille;
    }

    public int getCurentGrilleIndex() {
        return currentGrilleId;
    }
}
