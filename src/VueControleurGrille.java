import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.HashMap;
import java.util.Observable;
import java.util.Observer;

//Vue et Controleur

public class VueControleurGrille extends JFrame implements Observer {
    public static final int PIXEL_PER_SQUARE = 60;
    private Jeu game;
    // tableau de cases : i, j -> case
    private VueCase[][] tabCV;
    // hashmap : case -> i, j
    private HashMap<VueCase, Point> hashmap; // voir (*)
    // currentComponent : par défaut, mouseReleased est exécutée pour le composant (JLabel) sur lequel elle a été enclenchée (mousePressed) même si celui-ci a changé
    // Afin d'accéder au composant sur lequel le bouton de souris est relaché, on le conserve avec la variable currentComponent à
    // chaque entrée dans un composant - voir (**)
    private JComponent currentComponent;

    private BareDeMenu bm;
    private Panel panel;

    public VueControleurGrille(int size) {
        //taille par défault de 3
        game = new Jeu(size);
        //chargement de la grille de taille 3 ou 6
        game.loadGrille();
        game.addObserver(this);

        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        setSize(size * PIXEL_PER_SQUARE, size * PIXEL_PER_SQUARE);

        //ajout d'une barre de menu pour choisir

        bm = new BareDeMenu(game, this);
        setJMenuBar(bm);

        panel = new Panel(game, this);

        hashmap = new HashMap<VueCase, Point>();

        JPanel contentPane = new JPanel(new GridLayout(size, size));

        initGrille(contentPane, size);

        setContentPane(contentPane);

    }



    public void initGrille(JPanel j, int size) {
        tabCV = new VueCase[size][size];

        for (int x = 0; x < size; x++) {
            for (int y = 0; y < size; y++) {
                CaseM m = game.getCase(x, y);
                tabCV[x][y] = new VueCase(x, y, m);
                //System.out.println("x : " + i + " y : " + j + " type " + m.getType().toString());
                j.add(tabCV[x][y]);

                hashmap.put(tabCV[x][y], new Point(x, y));

                //pour chaque case qui est un JPanel on ajoute un MouseListener qu'on conigure
                tabCV[x][y].addMouseListener(new MouseAdapter() {
                    //dans le cas ou l'utilisateur presse la souris
                    @Override
                    public void mousePressed(MouseEvent e) {
                        Point p = hashmap.get(e.getSource()); // (*) permet de récupérer les coordonnées d'une caseVue
                        //game.rnd(p.x, p.y); //on modifie une case qui n'est pas numéroté à la position x et y

                        //transmet les coordonnés de la case vue au jeu
                        game.setIsPressed(true);
                        game.setCasePressed(p, m);
                        //((VueCase) e.getSource()).rndType();
                        //System.out.println("mousePressed : " + e.getSource() + " type " + m.getType());

                    }

                    //dans le cas ou l'utilisateur parcours une case
                    @Override
                    public void mouseEntered(MouseEvent e) {
                        // (**) - voir commentaire currentComponent
                        currentComponent = (JComponent) e.getSource();
                        Point p = hashmap.get(e.getSource());
                        //System.out.println("mouseEntered : " + e.getSource() + " type " + m.getType());
                        game.setCasePressed(p, m);

                    }

                    //dans le cas ou l'utilisateur relache le curseur de la souris
                    @Override
                    public void mouseReleased(MouseEvent e) {
                        // (**) - voir commentaire currentComponent
                        //System.out.println("mouseReleased : " + currentComponent + " type " + m.getType());

                        game.setIsPressed(false);
                        if (!game.cheminValide()) {
                            //System.out.println("supression du chemin courant " + game.getCheminActuelId());
                            game.videChemin();
                            game.afficheGrille();
                        }

                        //verifi si la partie est gagné, alors on modifie le panneau qui content la grille pour le remplacé par un bouton suivant
                        if (game.partieGagne()) {
                            //System.out.println("c'est gagné");
                            panel.load();
                            setContentPane(panel);
                        }


                    }
                });

            }
        }
    }

    public void switchGrille(int newType) {
        game.setTypeGrille(newType);

        int nouvelleTaille = game.getTailleGrille();

        //System.out.println("Nouvelle taille de la grille : " + nouvelleTaille);
        setSize(nouvelleTaille * VueControleurGrille.PIXEL_PER_SQUARE, nouvelleTaille * VueControleurGrille.PIXEL_PER_SQUARE);

        tabCV = new VueCase[nouvelleTaille][nouvelleTaille];

        JPanel contentPane = new JPanel(new GridLayout(nouvelleTaille, nouvelleTaille));

        initGrille(contentPane, nouvelleTaille);

        setContentPane(contentPane);
    }

    @Override
    public void update(Observable o, Object arg)
    {
        //System.out.println("je passe dans ce bout de code");
        for (int i=0; i < game.getTailleGrille(); i++) {
            for (int j=0; j < game.getTailleGrille(); j++) {
                tabCV[i][j].repaint();
            }
        }
    }


    public static void main(String[] args) {

        VueControleurGrille vue = new VueControleurGrille(6);
        vue.setVisible(true);

    }

}
