
JFLAGS = -g
JC = javac
OUTPUT = -d ./out

CLASSES	=	./out/BareDeMenu.class	\
			./out/CaseM.class	\
			./out/CaseType.class	\
			./out/Chemin.class	\
			./out/Jeu.class	\
			./out/Pair.class	\
			./out/Panel.class	\
			./out/VueCase.class	\
			./out/VueControleurGrille.class

SOURCES	=	./src/BareDeMenu.java	\
			./src/CaseM.java	\
			./src/CaseType.java	\
			./src/Chemin.java	\
			./src/Jeu.java	\
			./src/Pair.java	\
			./src/Panel.java	\
			./src/VueCase.java	\
			./src/VueControleurGrille.java

$(CLASSES): $(SOURCES)
	$(JC) $(OUTPUT) $(JFLAGS) $(SOURCES)

clean:
	rm -f ./out/*.class

exec: $(CLASSES)
	cd ./out; java VueControleurGrille



